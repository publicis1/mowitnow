package com.mowitnow;

import org.junit.Test;

import static org.junit.Assert.*;

public class ForwardTest {

    @Test
    public void testNorthStep() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.N);
        t.moveForward();

        assertEquals("(0, 1, N)", t.getPosition());
    }

    @Test
    public void testWestStep() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.W);
        t.moveForward();

        assertEquals("(-1, 0, W)", t.getPosition());
    }

    @Test
    public void testSouthStep() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.S);
        t.moveForward();

        assertEquals("(0, -1, S)", t.getPosition());
    }

    @Test
    public void testEastStep() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.E);
        t.moveForward();

        assertEquals("(1, 0, E)", t.getPosition());
    }

}
