package com.mowitnow;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class CommandeTest {

    @Test
    public void test() {
        try {
            String fileName = "C:\\Travail\\wks\\mowitnow\\src\\test\\resources\\ItTest.txt";

            Stream<String> lines = Files.lines(Paths.get(fileName));

            assertNotNull(lines);
            assertEquals("5 5", lines.findFirst().get());
        } catch (Exception e) {
            e.printStackTrace();
            fail("Exception: " + e.getMessage());
        }

    }

}
