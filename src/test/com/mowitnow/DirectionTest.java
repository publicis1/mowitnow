package com.mowitnow;

import org.junit.Test;

import static org.junit.Assert.*;

public class DirectionTest {

    @Test
    public void testNorthToLeft() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.N);
        t.moveLeft();

        assertEquals(Direction.W, t.getOrientation());

    }

    @Test
    public void testNorthToRight() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.N);
        t.moveRight();

        assertEquals(Direction.E, t.getOrientation());

    }

    @Test
    public void testWestToLeft() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.W);
        t.moveLeft();

        assertEquals(Direction.S, t.getOrientation());

    }

    @Test
    public void testWestToRight() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.W);
        t.moveRight();

        assertEquals(Direction.N, t.getOrientation());

    }

    @Test
    public void testSouthToLeft() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.S);
        t.moveLeft();

        assertEquals(Direction.E, t.getOrientation());

    }

    @Test
    public void testSouthToRight() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.S);
        t.moveRight();

        assertEquals(Direction.W, t.getOrientation());

    }

    @Test
    public void testEastToLeft() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.E);
        t.moveLeft();

        assertEquals(Direction.N, t.getOrientation());

    }

    @Test
    public void testEastToRight() {
        Tondeuse t = new Tondeuse(new Point(0, 0), Direction.E);
        t.moveRight();

        assertEquals(Direction.S, t.getOrientation());

    }

}
