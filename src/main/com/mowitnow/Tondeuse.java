/**
 *
 */
package com.mowitnow;

/**
 * @author Tidiane
 *
 */
public class Tondeuse {

    private static Rectangle limit;
    private final Point c;
    private Direction orientation;

    public Tondeuse(Point p, Direction dir) {
        this.c = p;
        this.orientation = dir;
    }

    public static Rectangle getLimit() {
        return limit;
    }

    public static void setLimit(Rectangle limit) {
        Tondeuse.limit = limit;
    }

    public void moveLeft() {
        this.orientation = orientation.getLeftDirection();
    }

    public void moveRight() {
        this.orientation = orientation.getRighttDirection();
    }

    public void moveForward() {
        int nextX = c.getX() + orientation.getDx();
        int nextY = c.getY() + orientation.getDy();

        if (limit != null && !limit.contains(nextX, nextY)) {
            return;
        }

        this.c.moveTo(new Point(nextX, nextY));
    }

    public Direction getOrientation() {
        return orientation;
    }

    public Point getCenter() {
        return c;
    }

    public String getPosition() {
        return "(" + c.getX() + ", " + c.getY() + ", " + orientation.name() + ")";
    }

}
