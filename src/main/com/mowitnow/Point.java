package com.mowitnow;

public class Point {

    private int x;
    private int y;

    public Point(int i, int j) {
        this.x = i;
        this.y = j;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void moveTo(Point point) {
        this.x = point.x;
        this.y = point.y;
    }

}
