package com.mowitnow;

public enum Commande {
    D() {
        @java.lang.Override
        public void execute(Tondeuse t) {
            t.moveRight();
        }
    },
    G() {
        @java.lang.Override
        public void execute(Tondeuse t) {
            t.moveLeft();
        }
    },
    A() {
        @java.lang.Override
        public void execute(Tondeuse t) {
            t.moveForward();
        }
    };

    public abstract void execute(Tondeuse t);

}
