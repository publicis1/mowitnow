package com.mowitnow;

public class Rectangle {
    private final int xMax;
    private final int xMin;
    private final int yMax;
    private final int yMin;

    public Rectangle(int xMax, int xMin, int yMax, int yMin) {
        super();
        this.xMax = xMax;
        this.xMin = xMin;
        this.yMax = yMax;
        this.yMin = yMin;
    }

    public Rectangle(Point p) {
        super();
        this.xMax = p.getX();
        this.yMax = p.getY();
        this.xMin = 0;
        this.yMin = 0;
    }

    public boolean contains(int x, int y) {
        return xMax >= x && xMin <= x &&
                yMax >= y && yMin <= y;
    }

}
