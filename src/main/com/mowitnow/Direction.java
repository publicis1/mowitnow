package com.mowitnow;

public enum Direction {
    N(0, 0, 1), W(1, -1, 0), S(2, 0, -1), E(3, 1, 0);

    private final int ord;

    private final int dx;

    private final int dy;

    private Direction(int ord, int dx, int dy) {
        this.ord = ord;
        this.dx = dx;
        this.dy = dy;
    }

    public static Direction getNewDirection(int dir) {
        dir %= 4;
        if (dir < 0)
            dir += 4;
        for (Direction d : Direction.values()) {
            if (d.ord == dir)
                return d;
        }
        throw new Error("Direction Not found for: " + dir);
    }

    int getDx() {
        return dx;
    }

    int getDy() {
        return dy;
    }

    public Direction getLeftDirection() {
        return getNewDirection(ord + 1);
    }

    public Direction getRighttDirection() {
        return getNewDirection(ord - 1);
    }
}