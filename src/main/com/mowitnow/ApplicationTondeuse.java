package com.mowitnow;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ApplicationTondeuse {

    public static void main(String[] args) {

        try {
            System.out.print("Donner le chemin complet du fichier de commandes (avec son extension): ");

            Scanner input = new Scanner(System.in);

            File file = new File(input.nextLine());

            List<String> lines = Files.lines(file.toPath()).collect(Collectors.toList());

            int[] coordonnees = Arrays.stream(lines.get(0).split(" ")).mapToInt(Integer::parseInt).toArray();
            Point pMax = new Point(coordonnees[0], coordonnees[1]);

            Map<String, String> commandeList = mapCommande(lines.subList(1, lines.size()));

            lancerCommande(commandeList, pMax);

            input.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void lancerCommande(Map<String, String> commandeList, Point haut) {
        commandeList.entrySet().forEach(e -> demarrerTondeuse(e.getKey(), e.getValue(), haut));
    }

    private static Tondeuse demarrerTondeuse(String key, String value, Point haut) {
        Tondeuse t = initTondeuse(key);
        Tondeuse.setLimit(new Rectangle(haut));
        String[] commandes = value.split("");
        executerCmd(t, commandes);
        System.out.println("Tondeuse position initiale: " + key);
        System.out.println("Commandes: " + value);
        System.out.println("Tondeuse position finale: " + t.getPosition());
        return t;
    }

    private static void executerCmd(Tondeuse t, String[] commandes) {
        Stream.of(commandes).forEach(c -> Commande.valueOf(c).execute(t));
    }

    private static Tondeuse initTondeuse(String pos) {
        String[] info = pos.split(" ");
        if (null == info || info.length < 3)
            return null;
        return new Tondeuse(new Point(Integer.parseInt(info[0]), Integer.parseInt(info[1])), Direction.valueOf(info[2]));
    }

    private static Map<String, String> mapCommande(List<String> lines) {
        return IntStream.range(0, (lines.size()) / 2)
                .boxed()
                .collect(Collectors
                        .toMap(i -> lines.get(2 * i),
                                i -> lines.get(2 * i + 1)));
    }

}
